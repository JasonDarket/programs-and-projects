#This program was written by Greg Dillon(me)
#This program's aim is to recreate the game 'Hangman' inside of a python
#environment.

import hangfunc
import random
import functools
from wordList import *


cont = 'y'
#debug
#hangfunc.gallows(7)
#print(easy)

print('''
  
sN     No    /mdh    .Mdd.  `M:  :ds:---+/  +Mhh`    /mdh    :Nhd`    Mdm-   N+
sN.....No   -N:.N+   .M:sm- `M: .No  `````  +M.ms   :m-hh   `m+`ms    M++m/  N+
sNoooooMo  `dy``+N-  .M: +m:`M: /M-  /oods  +M -N+ -m/ hh   hh``:M/   M+ /m+ N+ 
sN     No  smoooodm` .M:  /m+M: `my`    dy  +M  /N/mo  hh  +NoooohN-  M+  -moN+ 
om     mo :m-    `ds .N-   :dN:  .sys++odo  +m   oms   yy -m/     hh  m/   .hN+

                             YOU BEST BUCKLE UP

''')

input('''                          (press enter to continue)  
                                      ''') #MAKE INTO BLINKING TEXT

#CLEAR SCREEN

#PRINT OUT AT SET SPEED
print("intro screen")

while cont == 'y':
    #FIX MESSAGE
    print('''choose:
      1: easy way out
      2: easy
      3: medium
      4: hard''')

    choice = int(input("choice?  "))

    if choice == 1:
        #FIX MESSAGE
        print("A wise choice.")
        cont = 'n'
        
    if choice == 2:
        ezWord = random.choice(easy)
        easy.remove(ezWord)
        gameover = hangfunc.game(ezWord)
        
        if gameover == "win":
            print("you win")
        else:
            print("loooooooosssserrrrrr")
            
        #DEBUG  
        if ezWord in easy:
            print("Easy:failed extraction")
            
    if choice == 3:
        midWord = random.choice(medium)
        medium.remove(midWord)
        print(midWord)
        if midWord in easy:
            print("Mid:failed extraction")
            
    if choice == 4:
        hardWord = random.choice(hard)
        hard.remove(hardWord)
        print(hardWord)
        if hardWord in hard:
            print("Hard:failed extraction")

#CHECK FOR INPUT OUTSIDE OF Y/N.  IF IT IS, CHEEKY MESSAGE
        #FIX MESSAGE
cont = ("again? (y/n) ")










