#These are the words that the main file 'hangman' chooses from. Some are from searching the web,
#some are from Greg Dillon (me).

easy = ['baby', 'door', 'banana', 'finger', 'fence', 'big', 'sun', 'bag', 'big', 'church',
        'boy', 'girl', 'winter', 'summer', 'mouse', 'beach', 'king', 'water', 'jelly', 'eye',
        'balloon', 'ur mom', 'mud', 'drum', 'school', 'belt', 'shark', 'spider', 'computer',
        'pillow', 'gum', 'queen', 'dress', 'book', 'bed', 'pupper', 'doggo', 'meme', 'doctor']

medium = ['flagpole', 'batteries', 'banister', 'campsite', 'bedspread', 'windmill', 'nightmare',
          'wristwatch', 'gumball', 'sprinkler', 'bonnet', 'headache', 'saddle', 'shipwreck',
          'candestick', 'smog', 'hurdle', 'recycle', 'photographer', 'fiddle', 'easle', 'cello',
          'bowtie', 'dentist', 'cardboard', 'scoundrel', 'brainstorm', 'lol', 'kitchen', 'whatever']

hard = ['capitalism', 'reimbursment', 'escalator', 'sequins', 'unremitting', 'mispeled', 'sordid',
        'fortnight', 'labiodental', 'onerous', 'quarantine', 'addendum', 'flotsam', 'inquisition',
        'phonemic', 'ethylenediaminetetraacetic', 'alee', 'asea', 'stye', 'adamant', 'serried',
        'defenestrate', 'doggedly', 'cartilaginous', 'monophthongal', 'pharyngealized', 'conscienceless']
